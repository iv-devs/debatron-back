var mongoose = require('mongoose');

/*
@teams es un array de "ids" del documento Team
@participants debe llevar esta estructura:
{
		id_user:ObjectId,
		id_team:ObjectId
}
*/
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
module.exports = mongoose.model('debate',{
	id: String,
	id_category: ObjectId,
	id_creator_user: ObjectId,
	id_moderator_user: ObjectId,
	creation_date: Date,
	date: Date,	
	name: String,
	description: String,
	participants:Array,
	status:String,
	rules: {
		total_time: Number,
		time_per_user: Number
	},
	Tags: Array,
	Teams: Array
});