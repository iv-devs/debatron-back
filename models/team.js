// Mongoose import
var mongoose = require('mongoose');

module.exports = mongoose.model('team',{
	id: String,
	name: { type: String, unique: true },
	description: String,
	users:Array
});