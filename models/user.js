// Mongoose import
var mongoose = require('mongoose');

// Mongoose Model definition
module.exports = mongoose.model('user',{
    id: String,
    username: String,
    password: String,
    email: { type: String, unique: true },
    firstName: String,
    lastName: String,
    career: String,
    fb: {
        id: String,
        access_token: String,
        profileUrl: String
    },
    twitter: {
        id: String,
        token: String,
        username: String,
        displayName: String,
        lastStatus: String
    },
    team:Array

});