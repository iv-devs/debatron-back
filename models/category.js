// Mongoose import
var mongoose = require('mongoose');

module.exports = mongoose.model('category',{
	id: String,
	name: String,
	description: String	
});