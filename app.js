var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var socket_io = require('socket.io');

var dbConfig = require('./config/db');
var mongoose = require('mongoose');

// Connect to DB
mongoose.connect(dbConfig.url,function(res,err){
  if(err) console.log('ERROR connecting to : ' + dbConfig.url + '. ' + err);
  else{ 
    if(res === undefined) console.log('connected to ' + dbConfig.url );
    else console.log('ERROR connecting to : ' + dbConfig.url + ' : ' + res.errmsg );
  }

});

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'bower_components')));

// Configuring Passport
var passport = require('passport');
var expressSession = require('express-session');
// TODO - Why Do we need this key ?
app.use(expressSession({secret: 'mySecretKey', resave: true, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());

 // Using the flash middleware provided by connect-flash to store messages in session
 // and displaying in templates
 var flash = require('connect-flash');
 app.use(flash());

// Initialize Passport
var initPassport = require('./passport/init');
initPassport(passport);

// Socket.io
app.io   = socket_io();


var routes = require('./routes/index');
var auth   = require('./routes/auth')(passport);
var testDB = require('./routes/testDB');
var chat   = require('./routes/chat')(app.io);
var createDebate = require('./routes/createDebate');
var team   = require('./routes/team');

app.use('/', routes);
app.use('/auth', auth);
app.use('/chat',chat);
app.use('/testDB', testDB);
app.use('/chat', chat);
app.use('/createDebate', createDebate);
app.use('/team', team);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
