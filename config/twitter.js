// twitter app settings
module.exports = {
	'apikey' : '[apikey]',
	'apisecret' : '[apisecret]',
	'callbackUrl' : '[callbackUrl]'
}