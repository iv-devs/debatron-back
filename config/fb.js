// facebook app settings
module.exports = {
	'appID'		: '[appID]',
	'appSecret'	: '[appSecret]',
	'callbackUrl'	: '[callbackUrl]',
	'profileFields'	: ['id', 'birthday', 'email', 'gender', 'displayName','name','profileUrl']
}