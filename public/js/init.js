(function($){
	$(function(){
		$(".button-collapse").sideNav();
		$('.modal-trigger').leanModal();
		$('#aside').pushpin({ top:110, bottom:500 });

		$("#btnCreateDebate").click(function(){
			location.href = "./createDebate/";
		});
		
		$('select').select2({
			tags: true,
			multiple: true,
			placeholder: "Invite Users",
			ajax: {
				url: "./team/getUsers",
				type:'POST',
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						q: params.term,
						page: params.page
					};
				},
				processResults: function (data, params) {
					params.page = params.page || 1;
					return {
						results:data,
						pagination: {
							more: (params.page * 30) < data.total_count
						}
					};
				},
				cache: true
			}
		});

		$("#btnCreateTeam").click(function(e) {
			var teamName = $("#txfTeamName").val();
			var description = $("#txfDescription").val();
			var users = $("#sltAutocompleteUsers").val();

			$.post('./team/create', {teamName: teamName,description:description,users:users}, function(data, textStatus, xhr) {

			});

		});

	// end of document ready
}); 

// end of jQuery name space
})(jQuery);