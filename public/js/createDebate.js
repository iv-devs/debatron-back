function validateFields(){
	if($("#txfTitle").val() == ""){
		alert("Debe ingresar un título");
		return false;
	}

	if($("#txaDescription").val() == ""){
		alert("Debe ingresar una descripción");
		return false;
	}

	if($("#txaTags").val() == ""){
		alert("Debe ingresar al menos un tag");
		return false;
	}
	if($("input[type='radio'][name='typeDebate']:checked").val() == "" || $("input[type='radio'][name='typeDebate']:checked").val() == undefined){
		alert("Debe seleccionar un tito de debate");
		return false;
	}


	if($("input[type='radio'][name='visibility']:checked").val() == "" || $("input[type='radio'][name='visibility']:checked").val() == undefined){
		alert("Debe seleccionar la visibilidad del debate");
		return false;
	}

	return true;
}

(function($){
	$(function(){
		$(".button-collapse").sideNav();
		$('.modal-trigger').leanModal();
		$('#aside').pushpin({ top:110, bottom:500 });


		$("#btnCreateDebate").click(function(e){
			e.preventDefault();
			if(validateFields()){

				var title 		=	$("#txfTitle").val();
				var description =	$("#txaDescription").val();
				var tags 		=	$("#txaTags").val();
				var debateType 	=	$("input[type='radio'][name='typeDebate']:checked").val();
				var visibility 	=	$("input[type='radio'][name='visibility']:checked").val();
				var date 		=	null;


				$.post('./createDebate/create', 
				{
					title: title,
					description:description,
					tags:tags,
					debateType: debateType,
					visibility: visibility,
					date: date
				},
				function(data, textStatus, xhr) {

				});

			}
		});
		
		
});// end of document ready


})(jQuery);// end of jQuery name space