# Debatron-backend
## Pre-requisitos
- Instalar Git ( https://git-scm.com/downloads )
- Instalar NodeJS ( https://nodejs.org/en/download/ )
- Instalar Bower  ( npm install -g bower )
- Instalar nodemon (npm install -g nodemon)
 
## Pasos

- Clonar repositorio ( git clone https://github.com/iv-devs/debatron-back.git )
- Dirigise al directorio donde quedo clonado el proyecto `cd debatron-back`
- Ejecuta `npm install`
- Ejecuta `bower install`
- Ejecuta `npm start` o `node ./bin/www`
