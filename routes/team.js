var express = require('express');
var router = express.Router();
var Team = require('../models/team');
var User = require('../models/user');

router.get('/', function(req, res, next) {
	
	Team.find( function(err, teams) {
		if (err) return console.log(err);
		res.json(teams);
	});
	
});
router.post('/create', function(req, res) {	
	var teamName = req.body.teamName;
	var description = req.body.description;
	var users = req.body["users[]"];
	var team = new Team();

	
	Team.find({name:teamName}, function(err, teamValidation) {
		console.log(teamValidation);
		if(teamValidation.length === 0){
			team.name = teamName.trim();
			team.description = description.trim();
			team.users = users;

			team.save(function(err) {
				if (err){
					console.log('Error in Saving team: '+err);
					throw err;
				}
				console.log('Team Registration succesful');
				res.json({type:"ok",data:"ok"});
			});
		}else{
			res.json({type:"error",data:"El usuario ya existe"});
		}

		
	});
});
router.post('/getUsers', function(req, res) {	
	User.find({email:new RegExp(req.body.q, 'i')}, function(err, users) {
		if (err) return console.log(err);
		res.json(users.map(function(valor){
			return {
				"id":valor._id,
				"text":valor.email
			};
		}));
	});
	
});		

module.exports = router;
