var express = require('express');
var router = express.Router();



module.exports = function(passport){
	var successRedirect = '/chat';
	
	/* GET login page */
	router.get('/', function(req, res) {
		res.redirect('/auth/login');
	});
	
	/* GET login page. */
	router.get('/login', function(req, res) {
		// Display the Login page with any flash message, if any
		res.render('auth/login', { 
			title:'Login', 
			message: req.flash('message') 
		});
	});
	/* Handle Login POST */
	router.post('/login', passport.authenticate('login', {
		successRedirect: successRedirect,
		failureRedirect: '/auth/login',
		failureFlash : true  
	}));

	/* GET Registration Page */
	router.get('/signup', function(req, res){
		res.render('auth/register', { 
			title:'Registro', 
			message: req.flash('message')
		});
	});

	/* Handle Registration POST */
	router.post('/signup', passport.authenticate('signup', {
		successRedirect: successRedirect,
		failureRedirect: '/auth/signup',
		failureFlash : true  
	}));
	
	/* Handle Logout */
	router.get('/signout', function(req, res) {
		req.logout();
		res.redirect('/');
	});
	
	// route for facebook authentication and login
	// different scopes while logging in
	router.get('/facebook', 
		passport.authenticate('facebook', { 
			scope : ['email','public_profile','user_friends'] 
		})
	);

	// handle the callback after facebook has authenticated the user
	router.get('/facebook/callback',
		passport.authenticate('facebook', {
			successRedirect : successRedirect,
			failureRedirect : '/'
		})
	);

	// route for twitter authentication and login
	// different scopes while logging in
	router.get('/twitter', 
		passport.authenticate('twitter')
	);

	// handle the callback after facebook has authenticated the user
	router.get('/twitter/callback',
		passport.authenticate('twitter', {
			successRedirect : successRedirect,
			failureRedirect : '/'
		})
	);
	
	return router;
}