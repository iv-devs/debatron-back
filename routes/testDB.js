var express = require('express');
var router = express.Router();

var User = require('../models/user');
var Category =  require('../models/category');
var Debate = require('../models/debate');
var Team = require('../models/team');

var bCrypt = require('bcrypt-nodejs');

// Generates hash using bCrypt
var createHash = function(password){
	return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}

router.get('/', function(req, res, next) {
	res.render('testDB');
});


router.get('/getUsers', function(req, res, next) {
	console.log('getting users list')
	User.find( function(err, teams) {
		if (err) return console.log(err);
		res.json(teams);
	});
	
});

router.get('/createUser', function(req, res, next) {
	
	var newUser = new User();

	newUser.username = 'username1';
	newUser.password = createHash('password232');
	newUser.email = 'email1'; 
	newUser.firstName = 'firstName1';
	newUser.lastName = 'lastName1';

	newUser.save(function(err) {
		if (err){
			console.log('Error in Saving user: '+err);
			throw err;
		}
		console.log('User Registration succesful');
		res.json(newUser);
	});
});



router.get('/getCategorys', function(req, res, next) {
	
	Category.find( function(err, teams) {
		if (err) return console.log(err);
		res.json(teams);
	});
	
});

router.get('/createCategory', function(req, res, next) {
	
	var category = new Category();
	var random = Math.random();
	category.name = 'categoría ' + random;
	category.description = 'description ' + random;
	

	category.save(function(err) {
		if (err){
			console.log('Error in Saving category: '+err);
			throw err;
		}
		console.log('Category Registration succesful');
		res.json(category);
	});
});




router.get('/getTeams', function(req, res, next) {
	console.log('getting users list')
	Team.find( function(err, teams) {
		if (err) return console.log(err);
		res.json(teams);
	});
	
});

router.get('/createTeam', function(req, res, next) {
	

	var team = new Team();
	var random = Math.random();
	team.name = 'Team ' + random;
	team.description = 'description ' + random;
	

	team.save(function(err) {
		if (err){
			console.log('Error in Saving team: '+err);
			throw err;
		}
		console.log('Team Registration succesful');
		res.json(team);
	});
});




router.get('/getDebates', function(req, res, next) {
	console.log('getting users list')
	Debate.find( function(err, teams) {
		if (err) return console.log(err);
		res.json(teams);
	});
	
});

router.get('/createDebate', function(req, res, next) {
	var debate = new Debate();
	var random = Math.random();
	/**
		Obtengo datos aleatorios
		**/
		Category.find(function(err,dataCategory){
			var idCategoryRandom = dataCategory[Math.floor(Math.random() * dataCategory.length)]._id;
			

			Team.find(function(err,dataTeam){
				var allTeams =  dataTeam.map(function(obj){
					return obj._id;
				});

				var idTeamRandom = dataTeam[Math.floor(Math.random() * dataTeam.length)]._id;

				User.find(function(err,dataUser){
					var idUserRandom = dataUser[Math.floor(Math.random() * dataUser.length)]._id;
					var allParticipants = dataUser.map(function(obj){
						return {
							id_user:obj._id,
							id_team: dataTeam[Math.floor(Math.random() * dataTeam.length)]._id
						};
					});



					debate.name = 'debate ' + random;
					debate.description = 'description ' + random;
					debate.id_category = idCategoryRandom;
					debate.id_creator_user = idUserRandom;
					debate.id_moderator_user = idUserRandom;
					debate.creation_date = "2015-11-29";
					debate.date = "2015-11-29";
					debate.participants = allParticipants;
					debate.status = "En progreso";
					debate.rules = {
						total_time: random,
						time_per_user: random
					};
					debate.Tags = ["Tags 1" + random,"Tags 2" + random];
					debate.Teams = allTeams;

					debate.save(function(err) {
						if (err){
							console.log('Error in Saving debate: '+err);
							throw err;
						}
						console.log('Debate Registration succesful');
						res.json(debate);
					});

				});
});
});

});




module.exports = router;
