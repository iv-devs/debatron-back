var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Debatron, the most  important  discussion platform' });
});

module.exports = router;
