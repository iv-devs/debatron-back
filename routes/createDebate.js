var express = require('express');
var router = express.Router();
var Debate = require('../models/debate');
/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('createDebate', { title: 'Debatron, the most  important  discussion platform' });
});

router.post('/create', function(req, res, next) {
	var title 		=	req.body.title.trim();
	var description =	req.body.description.trim();
	var tags 		=	req.body.tags;
	var debateType 	=	req.body.debateType.trim();
	var visibility 	=	req.body.visibility.trim();
	var date 		=	req.body.date;
	if(title !== "" && description !== "" && tags !== "" && debateType !== "" && visibility !==""){
		debate.name = title;
		debate.description = description;
		debate.id_category = null;
		debate.id_creator_user = null;
		debate.id_moderator_user = null;
		debate.creation_date = "2015-11-29";
		debate.date = "2015-11-29";
		debate.participants = null;
		debate.status = "En progreso";
		debate.rules = {
			total_time: random,
			time_per_user: random
		};
		debate.Tags = ["Tags 1","Tags 2"];
		debate.Teams = allTeams;

		debate.save(function(err) {
			if (err){
				console.log('Error in Saving debate: '+err);
				throw err;
			}
			console.log('Debate Registration succesful');
			res.json(debate);
		});
	}else{
		res.json({type:"error",data:"error validation"});
	}
});
module.exports = router;
