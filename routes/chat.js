var express = require('express');
var router = express.Router();
var isAuthenticated = require('../middlewares/auth_mid');


module.exports = function (io){
	/* GET chat home page. */
	router.get('/', isAuthenticated, function(req, res, next) {
		res.render('chat', { title: 'Chat Room', username: req.user.username });
	});
	// Chatroom

	// usernames which are currently connected to the chat
	var usernames = {};
	var numUsers = 0;
	var colWordsPetition = [];
	var lastPetition = [];

	io.on('connection', function (socket) {
		console.log('connection');
		var addedUser = false;

		// when the client emits 'new message', this listens and executes
		socket.on('new message', function (data) {
			console.log('new message from [' + socket.username + '] -> ' + data);
			// we tell the client to execute 'new message'
			socket.broadcast.emit('new message', {
				username: socket.username,
				message: data
			});
			lastPetition = [];
		});

		// when the client emits 'add user', this listens and executes
		socket.on('add user', function (username) {
			console.log('add user [' + username + ']');
			// we store the username in the socket session for this client
			socket.username = username;
			// add the client's username to the global list
			usernames[username] = username;
			++numUsers;
			addedUser = true;
			socket.emit('login', {
				numUsers: numUsers
			});
			// echo globally (all clients) that a person has connected
			socket.broadcast.emit('user joined', {
				username: socket.username,
				numUsers: numUsers
			});
		});

		socket.on('user petition', function () {
			
			if(lastPetition.length < 1  ){
				lastPetition[0] = socket.username;
				socket.broadcast.emit('request word', {
					username: socket.username,
					actualUser: lastPetition[0],
				});
			}else{
				if(lastPetition[0] !== socket.username){					
					socket.broadcast.emit('request word', {
						username: socket.username,
						actualUser: lastPetition[0],
					});
				}
			}
			
		});
		// when the client emits 'typing', we broadcast it to others
		socket.on('typing', function () {
			socket.broadcast.emit('typing', {
				username: socket.username
			});
		});

		// when the client emits 'stop typing', we broadcast it to others
		socket.on('stop typing', function () {
			socket.broadcast.emit('stop typing', {
				username: socket.username
			});
		});

		// when the user disconnects.. perform this
		socket.on('disconnect', function () {
			// remove the username from global usernames list
			if (addedUser) {
				delete usernames[socket.username];
				--numUsers;

				// echo globally that this client has left
				socket.broadcast.emit('user left', {
					username: socket.username,
					numUsers: numUsers
				});
			}
		});
	});

	return router;
};